// BSD 3-Clause License
//
// Copyright (c) 2020, Corentin Risselin
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <cassert>
#include <cstdlib>

#include <algorithm>
#include <any>
#include <exception>
#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include <variant>
#include <vector>
#include <unordered_map>

#include <iostream>

namespace argparse
{
	enum Action {store, store_const, store_true, append, append_const, count, help, version};
	enum Nargs {optional='?', wildcard='*', multiple='+'};

	class ArgumentParser
	{
	private:
		class Argument
		{
		public:
			std::vector<std::string> names;
			Action action;
			std::variant<int, char, Nargs> nargs;
			std::any default_value;
			std::any const_value;
			bool required;
			std::string help;
		};

		class ParsedArgument
		{
		public:
			std::vector<std::string> name;
			std::any value;
		};

		class ParsedArguments
		{
		public:
			std::vector<ParsedArgument> _arguments;
			std::unordered_map<std::string, size_t> _arg_map;

			void register_arg(const std::vector<std::string> names, const std::any& value={})
			{
				for(auto& name: names)
					if(_arg_map.find(name) != _arg_map.end())
						throw std::runtime_error("Argument already registered");
				size_t current_index = _arguments.size();
				for(auto& key : names) _arg_map[key] = current_index;
				_arguments.push_back({names, value});
			}

			bool has(const std::string& key) { return _arg_map.find(key) != _arg_map.end(); }

			std::any operator[](const std::string& key)
			{
				auto result = _arg_map.find(key);
				if(result == _arg_map.end())
					throw std::runtime_error("Argument not found");
				return _arguments[result->second].value;
			}

			std::string getString(const std::string& key)
			{
				auto result = _arg_map.find(key);
				if(result == _arg_map.end())
					throw std::runtime_error("Argument not found");

				auto value = _arguments[result->second].value;
				if(value.type() == typeid(std::string)) return std::any_cast<std::string>(value);
				else if(value.type() == typeid(char*)) return {std::any_cast<char*>(value)};
				else if(value.type() == typeid(const char*)) return {std::any_cast<const char*>(value)};
				else if(value.type() == typeid(int)) return std::to_string(std::any_cast<int>(value));
				else if(value.type() == typeid(double)) return std::to_string(std::any_cast<double>(value));
				else if(value.type() == typeid(float)) return std::to_string(std::any_cast<float>(value));
				throw std::runtime_error("Cannot parse argument");
			}

			bool getBool(const std::string& key)
			{
				auto result = _arg_map.find(key);
				if(result == _arg_map.end()) return false;

				auto value = _arguments[result->second].value;
				if(value.type() == typeid(bool)) return std::any_cast<bool>(value);
				else if(value.type() == typeid(std::string)) return std::any_cast<std::string>(value) != "0";
				else if(value.type() == typeid(char*)) return std::any_cast<char*>(value)[0] != '0';
				else if(value.type() == typeid(const char*)) return std::any_cast<const char*>(value)[0] != '0';
				throw std::runtime_error("Cannot parse argument");
			}

			int getInt(const std::string& key)
			{
				auto result = _arg_map.find(key);
				if(result == _arg_map.end())
					throw std::runtime_error("Argument not found");

				auto value = _arguments[result->second].value;
				if(value.type() == typeid(int)) return std::any_cast<int>(value);
				else if(value.type() == typeid(unsigned int)) return std::any_cast<unsigned int>(value);
				else if(value.type() == typeid(unsigned long)) return std::any_cast<unsigned long>(value);
				else if(value.type() == typeid(unsigned short)) return std::any_cast<unsigned short>(value);
				else if(value.type() == typeid(std::string)) return stoi(std::any_cast<std::string>(value));
				else if(value.type() == typeid(char*)) return atoi(std::any_cast<char*>(value));
				else if(value.type() == typeid(const char*)) return atoi(std::any_cast<const char*>(value));
				else if(value.type() == typeid(float)) return (int)std::any_cast<float>(value);
				else if(value.type() == typeid(double)) return (int)std::any_cast<double>(value);
				throw std::runtime_error("Cannot parse argument");
			}

			float getFloat(const std::string& key)
			{
				auto result = _arg_map.find(key);
				if(result == _arg_map.end())
					throw std::runtime_error("Argument not found");

				auto value = _arguments[result->second].value;
				if(value.type() == typeid(double)) return (float)std::any_cast<double>(value);
				else if(value.type() == typeid(float)) return std::any_cast<float>(value);
				else if(value.type() == typeid(std::string)) return stof(std::any_cast<std::string>(value));
				else if(value.type() == typeid(char*)) return atof(std::any_cast<char*>(value));
				else if(value.type() == typeid(const char*)) return atof(std::any_cast<const char*>(value));
				else if(value.type() == typeid(int)) return (float)std::any_cast<int>(value);
				throw std::runtime_error("Cannot parse argument");
			}
		};

		std::string _description;
		std::string _epoligue;
		std::string _prog;
		std::string _usage;

		std::vector<Argument> _arguments;
		std::vector<size_t> _positional_args;
		std::vector<size_t> _keyword_args;
		std::unordered_map<std::string, size_t> _arg_map;

	public:
		ArgumentParser(
			const std::string& prog="program",
			const std::string& description="",
			const std::string& epilogue="",
			const std::string& usage="")
		{
			_prog = prog;
			if(!description.empty()) _description = description;
			if(!epilogue.empty()) _epoligue = epilogue;
			if(!usage.empty()) _usage = usage;

			add_argument({"-h", "--help"}, "show this help message and exit", Action::store_true);
		}

		void print_help()
		{
			std::stringstream ss;
			ss << "usage: " << _prog;

			// Printing optional arguments
			for(auto& arg_index : _keyword_args)
			{
				auto& arg = _arguments[arg_index];
				ss << " [" << arg.names[0];
				if((arg.action != Action::store) && (arg.action != Action::store_const) && (arg.action != Action::store_true))
				{
					if(std::holds_alternative<int>(arg.nargs) == true)
					{
						for(int i = 0; i < std::get<int>(arg.nargs); i++) ss << " " << std::uppercase << arg.names[0];
					}
					else
						ss << " " << std::uppercase << arg.names[0];
				}
				ss << "]";
			}
			// Printing positional arguments
			for(auto& arg_index : _positional_args)
			{
				auto& arg = _arguments[arg_index];
				if((arg.action != Action::store_const) && (arg.action != Action::store_true))
				{
					if(std::holds_alternative<int>(arg.nargs) == true)
					{
						for(int i = 0; i < std::get<int>(arg.nargs); i++) ss << " " << arg.names[0];
					}
					else
						ss << " " << arg.names[0];
				}
			}
			std::cout << ss.str() << std::endl;
		}

		void print_additional_help()
		{
			std::stringstream ss;
			for(auto& [arg_type, arg_indices] : std::vector<std::tuple<std::string, std::vector< size_t >>>{
				{"positional arguments", _positional_args}, {"optional arguments", _keyword_args}})
			{
				ss << "\n" << arg_type << ":\n";
				for(auto& arg_index : arg_indices)
				{
					auto& arg = _arguments[arg_index];
					bool first_print = true;
					std::stringstream arg_line;

					arg_line << "  ";
					for(auto& name: arg.names)
					{
						if(first_print) first_print = false;
						else arg_line << ", ";
						arg_line << name;
					}

					if(!arg.help.empty())
					{
						if(arg_line.str().size() < 23)
							for(size_t len=0; len < arg_line.str().size() - 24; len += 1) arg_line << " ";
						else
							arg_line << "\n                        ";
						arg_line << arg.help;
					}
					ss << arg_line.str() << "\n";
				}
			}
			std::cout << ss.str() << std::flush;
		}

		void add_argument(
			const std::vector<std::string>& names,
			const std::string& help,
			const std::any& default_value,
			const bool required)
		{
			add_argument(names, help, Action::store, Nargs::optional, default_value, {}, required);
		}

		void add_argument(
			const std::vector<std::string>& names,
			const std::string& help="",
			const Action& action=Action::store,
			const std::variant<int, char, Nargs>& nargs=Nargs::optional,
			const std::any& default_value={},
			const std::any& const_value={},
			const bool required=false)
		{
			if(names.size() == 0)
				throw std::runtime_error("argument name missing");
			size_t arg_index = _arguments.size();

			// Argument is position or keyworded
			bool is_positional = names[0][0] != '-';
			if(is_positional) _positional_args.push_back(arg_index);
			else _keyword_args.push_back(arg_index);

			for(auto& arg_key : names)
			{
				if( _arg_map.find(arg_key) != _arg_map.end())
				{
					std::stringstream ss;
					ss << "Arguments name \"" << arg_key << "\" is already registered";
					print_help();
					throw std::runtime_error(ss.str());
				}
				_arg_map[arg_key] = arg_index;
			}
			_arguments.push_back({names, action, nargs, default_value, const_value, required, help});
		}

		ParsedArguments parse_args(int argc, char** argv)
		{
			ParsedArguments parsed_args;
			size_t positional_index = 0;
			std::vector<std::string> missing_arguments;
			std::vector<std::string> unrecognized_arguments;

			for(int arg_index=1; arg_index < argc; arg_index += 1)
			{
				std::string arg_value {argv[arg_index]};
				bool is_positional = arg_value[0] != '-';
				if(is_positional)
				{
					if(positional_index >= _positional_args.size())
					{
						unrecognized_arguments.push_back(arg_value);
						continue;
					}

					auto& current_arg = _arguments[_positional_args[positional_index]];
					parsed_args.register_arg(current_arg.names, arg_value);

					positional_index += 1;
				}
				else
				{
					auto arg_search = _arg_map.find(arg_value);
					if(arg_search == _arg_map.end())
					{
						unrecognized_arguments.push_back(arg_value);
						continue;
					}
					auto& current_arg = _arguments[arg_search->second];

					switch(current_arg.action)
					{
						case Action::store:
							arg_index += 1;
							if(arg_index >= argc || argv[arg_index][0] == '-')
							{
								std::stringstream ss;
								ss << _prog << ": error: argument " << arg_value << ": expected one argument";
								throw std::runtime_error(ss.str());
							}
							parsed_args.register_arg(current_arg.names, std::string(argv[arg_index]));
							break;
						case Action::store_const:
							parsed_args.register_arg(current_arg.names, current_arg.const_value);
							break;
						case Action::store_true:
							parsed_args.register_arg(current_arg.names, true);
							break;
						default:
							break;
					}
				}
			}

			for(auto& arg : _arguments)
				if(!parsed_args.has(arg.names[0]) && arg.default_value.has_value() && !arg.required)
					parsed_args.register_arg(arg.names, arg.default_value);

			if(parsed_args.has("--help"))
			{
				print_help();
				print_additional_help();
				exit(0);
			}

			// Getting missing positional arguments
			if(positional_index < _positional_args.size())
				for(size_t arg_index=positional_index; arg_index < _positional_args.size(); arg_index += 1)
					missing_arguments.push_back(_arguments[_positional_args[arg_index]].names.back());
			// Getting missing keyworded arguments
			for(auto arg_index: _keyword_args)
				if(_arguments[arg_index].required && !parsed_args.has(_arguments[arg_index].names[0]))
					missing_arguments.push_back(_arguments[arg_index].names.back());

			if(!missing_arguments.empty())
			{
				print_help();

				std::stringstream ss;
				bool first_arg_printed = false;
				ss << _prog << ": error: the following arguments are required: ";
				for(auto& arg_name: missing_arguments)
				{
					if(first_arg_printed) ss << ", ";
					else first_arg_printed = true;
					ss << arg_name;
				}
				throw std::runtime_error(ss.str());
			}

			if(!unrecognized_arguments.empty())
			{
				print_help();

				std::stringstream ss;
				bool first_arg_printed = false;
				ss << _prog << ": error: unrecognized arguments: ";
				for(auto& arg_name : unrecognized_arguments)
				{
					if(first_arg_printed) ss << " ";
					else first_arg_printed = true;
					ss << arg_name;
				}
				throw std::runtime_error(ss.str());
			}

			return parsed_args;
		}
	};
}
